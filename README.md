# Transit gap 🚌 🏙️

We know that some cities have a better public transport service than others, but is this _quantifiable_?

Here I propose an approach based on a route provider (Bing Maps, but it is easy to change that modifying the functions in `geo_utils.py`; you need an api key in a file called `bing_key.txt` if you want to run this).

First, a city is defined, providing to the class `City` the coordinates of the center, and four "boundary points". 
Then, a number of random locations are chosen inside a given radius from the center (and included in the boundaries); finally, origin-destination couples are chosen among them, and a route is queried from the provider at a random hour in the given range (I chose 4 to 6PM, because it is a significative rush-hour interval; it made sense to choose one, because queries are slow and limited, at least with my free API key). 
This happens both for driving and for transit (public transport).

The provider responds a route with the distance, the duration, and the instructions. __Caveat__:
Bing Maps does not always support real-time route planning; unfortunately, this is the case for the cities that I decided to consider. These results are then _not taking into account delays_. It is a big miss, but some results are already visible.

The results are visualized in different ways, to understand which cities are "faster": as histograms of the speed; as scatter plots of the speed against the distance.

The scatter plot shows a trend for Vienna in having faster trips.
![scatter](img/scatter_transit.png)

This trend is more clear looking at the histogram, where the average speed is higher than in Torino and Roma; even more interesting is noticing that the speeds in Vienna are spreaded on a wider range on the right, meaning that many are fast (around 15km/h), but very few are slow (around 7km/h) as opposite to Torino and Roma. 
![histogram](img/hist_transit.png)

Notice that there are trips at a constant speed (~5km/h), no matters what the distance is: these are trips that Bing Maps reports as faster to be travelled walking, and this is the estimated walking speed. In this analysis, they are just ignored, as they would falsificate the average.