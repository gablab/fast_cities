"""
geo_utils.py
This module contains some utilities for interacting with Bing Maps.
"""


import math
import numpy as np
import pandas as pd

import requests
from bokeh.plotting import figure
from bokeh.tile_providers import OSM, CARTODBPOSITRON, get_provider
from bokeh.io import output_notebook, show
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource
from bokeh.transform import factor_cmap, factor_mark
from bokeh.palettes import Spectral4
output_notebook()

import geopy.distance

from urly import URL

# get the local Bing key
#BING_KEY = str()
try:
    with open('bing_key.txt', 'r') as f:
        BING_KEY = f.read()
except FileNotFoundError:
    print('Error: missing bing_key.txt file!')

def bing_get_route_stats_from_coord(coord_1, coord_2,
                                    query_time=pd.Timestamp.today(),
                                    means='Transit', json=False):
    """
    Get route data providing two addresses as coordinates.
    Coordinates must be provided in wgs84 (lon, lat) format.
    """
    addr_1 = bing_get_address_from_coord(*coord_1)
    addr_2 = bing_get_address_from_coord(*coord_2)
    return bing_get_route_stats_from_addr(addr_1, addr_2,
                                          query_time=query_time,
                                          means=means, json=json)

def bing_get_route_stats_from_addr(addr_1, addr_2,
                                   query_time=pd.Timestamp.today(),
                                   means='Transit', json=False):
    """
    Get route data providing two addresses as strings.
    """
    addr_1 = addr_1.replace(',', '%2C').replace(' ', '%20')
    addr_2 = addr_2.replace(',', '%2C').replace(' ', '%20')
    url = URL('https://dev.virtualearth.net/REST/V1/Routes/'+str(means)+
              '?wp.0='+str(addr_1)+'&wp.1='+str(addr_2)+
              '&timeType=Departure&dateTime='+
              str(convert_to_bing_time(query_time))+
              '&key=%20AgBB8WfHojVumlPM2wQwUkn6XCbWtG4R8ovDo9FNeRLZchsrdJgDtSIiW6mzZekZ')
    got = requests.get(url)
    try:
        if json:
            return got.json()
        route_res = got.json()['resourceSets'][0]['resources'][0]
        departure_time = convert_from_bing_api_time(route_res['routeLegs'][0]['startTime'])
        arrival_time = convert_from_bing_api_time(route_res['routeLegs'][0]['endTime'])
        return {
            'travel_duration': route_res['travelDuration'],
            'distance': route_res['travelDistance'],
            'query_time': query_time,
            'departure_time': departure_time,
            'arrival_time': arrival_time,
            'actual_duration': arrival_time - query_time
        }
    except:
        print("ERROR: got"+
              ', '.join([str(got), str(means), str(addr_1),
                         str(addr_2),
                         str(convert_to_bing_time(query_time))]))
        return False

def bing_get_address_from_coord(lon, lat, json=False, locality=False):
    """
    Query Bing Maps to get address of (lon, lat) coordinates as a string.
    """
    url = 'http://dev.virtualearth.net/REST/v1/Locations/'+str(lat)+','+str(lon)+'?key='+BING_KEY
    got = requests.get(url)
    resources = got.json()['resourceSets'][0]['resources'][0]
    if locality:
        return resources['address']['locality']
    if json:
        return resources['address']
    return resources['address']['formattedAddress'].replace(',', '%2C').replace(' ', '%20')



def get_city_figure(city):
    """
    Get a tiled figure providing a City object.
    """
    lon_limits = [city.min_lon, city.max_lon]
    lat_limits = [city.min_lat, city.max_lat]
    return get_tiled_figure(lon_limits, lat_limits, title=city.name)

def get_tiled_figure(lon_limits, lat_limits, title=None, hover=None):
    """
    Get a tiled figure longitude and latitude limits.
    """
    tile_provider = get_provider(OSM)
    # supply range bounds in web mercator coordinates
    tools = ["pan", "wheel_zoom,box_zoom,reset"]
    if hover is not None:
        tools.append(hover)
    fig = figure(
        title=title,
        x_range=(wgs84_lon_to_webmercator(lon_limits[0]),
                 wgs84_lon_to_webmercator(lon_limits[1])),
        y_range=(wgs84_lat_to_webmercator(lat_limits[0]),
                 wgs84_lat_to_webmercator(lat_limits[1])),
        x_axis_type="mercator", y_axis_type="mercator",
        match_aspect=True, tools=tools
        )
    fig.add_tile(tile_provider)
    return fig



K_GEO_CONVERT = 6378137

def webmercator_to_wgs84_lon(x):
    return 180.0*x/(K_GEO_CONVERT*np.pi)

def webmercator_to_wgs84_lat(y):
    return 360.0/np.pi * np.arctan(np.exp(y/K_GEO_CONVERT)) - 90

def wgs84_lon_to_webmercator(lon):
    return lon * (K_GEO_CONVERT * np.pi/180.0)

def wgs84_lat_to_webmercator(lat):
    return np.log(np.tan((90 + lat) * np.pi/360.0)) * K_GEO_CONVERT


class Geocoo:
    """
    A class wrapping geographical coordinates, plus methods for conversion to different systems.
    """
    def __init__(self, coords, kind='wgs84', name='', center=None):
        self.name = name
        if kind == 'wgs84':
            self._lon = coords[0]
            self._lat = coords[1]
            self._conv_wgs84_to_wmerc()
        elif kind == 'webmercator':
            self._x = coords[0]
            self._y = coords[1]
            self._conv_wmerc_to_wgs84()
        elif kind == 'local':
            self._alpha = coords[0]
            self._beta = coords[1]
        else:
            raise Exception('Geocoo coordinates must be provided in wgs84 or webmercator system')
        if center is not None:
            self.add_center(center, kind=kind)
        self.properties = {}

    def add_center(self, center, kind):
        """
        sAdd center from which to compute distance.
        """
        self._center = center
        if kind == 'wgs84':
            self._conv_wgs84_to_local()
        elif kind == 'local':
            self._conv_local_to_wgs84()
            self._conv_wgs84_to_wmerc()

    def _conv_wgs84_to_wmerc(self):
        self._x = self._lon * (K_GEO_CONVERT * np.pi/180.0)
        self._y = np.log(np.tan((90 + self._lat) * np.pi/360.0)) * K_GEO_CONVERT

    def _conv_wmerc_to_wgs84(self):
        self._lon = 180.0*self._x/(K_GEO_CONVERT*np.pi)
        self._lat = 360.0/np.pi * np.arctan(np.exp(self._y/K_GEO_CONVERT)) - 90

    def _conv_wgs84_to_local(self):
        delta_lon = 0.01
        udm_lon = geopy.distance.distance(self.center.get_coords_wgs84(),
                                          self.center+[delta_lon, 0])
        self._alpha = self._lon / delta_lon * udm_lon
        delta_lat = 0.01
        udm_lat = geopy.distance.distance(self.center.get_coords_wgs84(),
                                          self.center+[0, delta_lat])
        self._beta = self._lat / delta_lat * udm_lat

    def _conv_local_to_wgs84(self):
        delta_lon = 0.01
        udm_lon = geopy.distance.distance(self.center.get_coords_wgs84(),
                                          self.center+[delta_lon, 0])
        self._lon = self._alpha * delta_lon / udm_lon
        delta_lat = 0.01
        udm_lat = geopy.distance.distance(self.center.get_coords_wgs84(),
                                          self.center+[0, delta_lat])
        self._lat = self._beta * delta_lat / udm_lat


    def get_coords_wgs84(self):
        return (self._lon, self._lat)

    def get_coords_wmerc(self):
        return (self._x, self._y)

    def get_coords_bull(self):
        return (self._alpha, self._beta)

    def __add__(self, other, kind="wgs84"):
        if kind == 'wgs84':
            return [self._lon + other[0], self._lat+other[1]]
        elif kind == 'webmercator':
            return [self._x + other[0], self._y + other[1]]
        else:
            raise Exception('Geocoo coordinates must be provided in wgs84 or webmercator system')



class City:
    """
    An object representing a city, with boundaries.
    """

    def __init__(self, center, south, north, west, east, name, kind='wgs84'):
        self.center = Geocoo(center, name=name, kind=kind)
        self.min_lon = west[0]
        self.max_lon = east[0]
        self.min_lat = south[1]
        self.max_lat = north[1]
        self.nw = Geocoo([west[0], north[1]], name=name+'_nw', kind=kind)
        self.se = Geocoo([east[0], south[1]], name=name+'_se', kind=kind)
        self.name = name
        self.locality = bing_get_address_from_coord(*self.center.get_coords_wgs84(), locality=True)

    def get_random_place(self, distance=None):
        """
        Get a random place in the city.
        """
        if distance is None:
            locality_rand = ''
            while locality_rand != self.locality:
                lon_rand = np.random.random() * (self.max_lon - self.min_lon) + self.min_lon
                lat_rand = np.random.random() * (self.max_lat - self.min_lat) + self.min_lat
                locality_rand = bing_get_address_from_coord(lon_rand, lat_rand, locality=True)
        else:
            distance_rand = distance + 1000
            while distance_rand > distance:
                lon_rand = np.random.random() * (self.max_lon - self.min_lon) + self.min_lon
                lat_rand = np.random.random() * (self.max_lat - self.min_lat) + self.min_lat
                cntr = self.center.get_coords_wgs84()[::-1]
                distance_rand = geopy.distance.distance((lat_rand, lon_rand), cntr)
                #locality_rand = bing_get_address_from_coord(lon_rand, lat_rand, locality=True)
        return Geocoo([lon_rand, lat_rand], kind='wgs84')

    def get_random_trips(self, n_places,
                         n_trips, means='Transit',
                         distance=None, hour_interval=[0, 24]):
        """
        Query data for `n_trips` random routes between `n_places` random
        points in the city. If `distance` is provided, the points are
        rejected until the distance from the city center is less.
        """
        self.places = [self.get_random_place(distance=distance) for i in range(n_places)]
        for pl in self.places:
            pl.properties['speeds'] = []
        #np.random.shuffle(places)
        trips = []
        for i in range(n_trips):
            route_stats = False
            while route_stats == False:
                ip1 = None
                ip2 = None
                while ip1 == ip2:
                    ip1, ip2 = np.random.choice(range(len(self.places)), 2)
                pl1 = self.places[ip1]
                pl2 = self.places[ip2]
                time_ini = get_random_hour(*hour_interval)
                # bing_get_route_stats_from_coord returns False if error
                route_stats = bing_get_route_stats_from_coord(
                    pl1.get_coords_wgs84(), pl2.get_coords_wgs84(),
                    query_time=(time_ini),
                    means=means
                )
            dist = route_stats['distance']
            durat = route_stats['actual_duration'].total_seconds()
            if durat == 0:
                print(route_stats, pl1.get_coords_wgs84(), pl2.get_coords_wgs84(), durat)
            if durat == pd.Timedelta('0h'):
                print(route_stats, pl1.get_coords_wgs84(), pl2.get_coords_wgs84(), durat)
            speed = dist/durat*3600
            trips.append({
                'start': pl1,
                'end': pl2,
                'distance': dist,
                'just_travel_duration': route_stats['travel_duration'],
                'duration': durat/3600,
                'speed_km_h': speed,
                'query_time': time_ini,
                'locality': self.locality,
                'means': means
            })
            pl1.properties['speeds'].append(speed)
            pl2.properties['speeds'].append(speed)
        return trips

def get_random_hour(start, end):
    """
    Returns random hour in the interval, for the current day:
    start and end are integers, can be in different days; 24h format.
    """
    if end < start:
        end += 24
    delta = end-start
    now_date = pd.to_datetime(pd.Timestamp.today().date())
    rand_delta = pd.Timedelta(f"{start}h") + pd.Timedelta(f"{delta}h")*np.random.random()
    return now_date + rand_delta

def convert_to_bing_time(date):
    """
    Convert a pandas.Timestamp to "hh:mm:ssAM"/"hh:mm:ssPM".
    """
    hour = date.hour
    if hour > 11:
        hour -= 12
        merid = 'PM'
    else:
        merid = 'AM'
    return f"{hour:02d}:{date.minute:02d}:{date.second:02d}"+merid

def convert_from_bing_api_time(code):
    """
    Convert the date returned by Bing API to a pandas.Timestamp.
    """
    code = code.split('(')[1].split(')')[0]
    if '+' in code:
        mult = int(+1)
        date_code, time_zone = code.split('+')
    else:
        mult = int(-1)
        date_code, time_zone = code.split('-')
    dh = int(time_zone[:-2])
    time_delta = pd.Timedelta(f"{dh}h")
    code_time = pd.to_datetime(int(date_code)* 1e6) + mult * time_delta
    return code_time
    #.tz_localize('Japan')
    #code_time, code_time.tz_convert('Europe/Rome')

def row_stats_trips(df_trips):
    """
    Return a row of scatter plots, one for each means of transport.
    """
    locality_cmap = factor_cmap('locality', palette=Spectral4,
                                factors=sorted(df_trips.locality.unique()))
    means_mark = factor_mark('means', markers=['circle', 'triangle'],
                             factors=sorted(df_trips.means.unique()))
    df_transit = df_trips.loc[df_trips['means'] == 'Transit',
                              ['distance', 'speed_km_h',
                               'duration', 'locality', 'means']]
    fig1 = figure(title='Transit routes', x_axis_label='distance [km]',
                  y_axis_label='speed [km/h]', x_range=[0, 15])
    fig1.scatter(x='distance', y='speed_km_h', size=10,
                 marker=means_mark, fill_color=locality_cmap,
                 line_color=None, fill_alpha=0.6,
                 source=df_transit, legend_group='locality')
    df_driving = df_trips.loc[df_trips['means'] == 'Driving',
                              ['distance', 'speed_km_h',
                               'duration', 'locality', 'means']]
    fig2 = figure(title='Driving routes',
                  x_axis_label='distance [km]',
                  y_axis_label='speed [km/h]')
    fig2.scatter(x='distance', y='speed_km_h',
                 size=10, marker=means_mark,
                 fill_color=locality_cmap, line_color=None, fill_alpha=0.6,
                 source=df_driving, legend_group='locality')
    return[fig1, fig2]
